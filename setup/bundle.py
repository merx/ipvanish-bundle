"""
setup/bundle.py
"""

import os


def _read_ovpn_config(ovpn_path: str, certificate: str) -> str:
    """

    :param ovpn_path:
    :param certificate:
    :return:
    """
    ovpn_content = ''
    # Get the content of the configuration as a string
    with open(ovpn_path, 'r') as open_file:
        lines = open_file.read()

        for line in lines.split('\n'):
            if 'ca ca.ipvanish.com.crt' in line:
                ovpn_content += '<ca>\n'
                ovpn_content += certificate
                ovpn_content += '</ca>\n'
                continue

            ovpn_content += line + '\n'
    return ovpn_content


def _read_certificate(cert_path: str) -> str:
    """

    :param cert_path:
    :return:
    """
    with open(cert_path) as f:
        cert = f.read()
    return cert


def _write_country_file(file_path, country_configs):
    """

    :param file_path:
    :param country_configs:
    :return:
    """
    with open(file_path, mode='w', encoding='utf-8') as _f:
        for key in country_configs:
            _f.write(f'\n\n{key} = ' + str(country_configs[key]))


def main():
    """

    :return:
    """
    # Get the base path
    base_dir = os.path.expanduser('~')

    # Set the directory containing the OpenVPN configuration files
    vpn_dir = os.path.join(base_dir, 'IPVanish')
    if not os.path.isdir(vpn_dir):
        raise Exception(f'Directory: {vpn_dir} not found')

    cert_path = os.path.join(vpn_dir, 'ca.ipvanish.com.crt')
    if not os.path.exists(cert_path):
        raise Exception(f'Certificate: {cert_path} not found')

    cert = _read_certificate(cert_path)

    bundle_dir = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
                              'ipvanish_bundle/bundle')
    if os.path.isdir(bundle_dir):
        raise Exception('Bundle directory does not exist')

    configs = {
        'linux': {},
        'darwin': {}
    }

    # Get the .ovpn files from the directory
    for _f in os.listdir(vpn_dir):
        if not _f.endswith('.ovpn') or _f.startswith('.'):
            continue

        # Get the country code from the file name
        try:
            f_country = _f.split('-')[1]
        except IndexError:
            raise

        if f_country not in configs['linux']:
            configs['linux'][f_country] = {}

        if f_country not in configs['darwin']:
            configs['darwin'][f_country] = {}

        ovpn_content = _read_ovpn_config(os.path.join(vpn_dir, _f), cert)
        mac_content = ovpn_content + 'pull\nfast-io\n'
        mac_lines = mac_content.split('\n')

        linux_content = ovpn_content + '\nscript-security 2\nup /etc/openvpn/update-resolv-conf\ndown /etc/openvpn/update-resolv-conf\n'
        linux_lines = linux_content.split('\n')

        ovpn_var_name = _f.replace('-', '_').replace('.', '_')

        if ovpn_var_name not in configs['darwin'][f_country]:
            configs['darwin'][f_country][ovpn_var_name] = mac_lines

        if ovpn_var_name not in configs['linux'][f_country]:
            configs['linux'][f_country][ovpn_var_name] = linux_lines

    for platform in configs:
        platform_dir = os.path.join(bundle_dir, platform)

        for country in configs[platform]:
            country_file = os.path.join(platform_dir, f'{country}.py')

            _write_country_file(country_file, configs[platform][country])


if __name__ == '__main__':
    main()
