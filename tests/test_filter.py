#!/usr/bin/env python3

import logging
import unittest

from ipvanish_bundle.core import query


class TestFilter(unittest.TestCase):

    def setUp(self):
        self.country = 'us'
        self.exclude = [
            'ipvanish_US_Los_Angeles_lax_a16_ovpn',
            'ipvanish_US_New_York_nyc_a51_ovpn'
        ]
        self.config = 'ipvanish_US_Ashburn_iad_a28_ovpn'

    def test_filter(self):
        filtered = query.filter(self.country, exclude=self.exclude)

        for elem in self.exclude:
            self.assertTrue(elem not in filtered)

    def test_get_by_country_name(self):
        cont = query.get_by_country_name(self.country, self.config)
        self.assertIsNotNone(cont)


if __name__ == "__main__":
    # disable logging
    logging.basicConfig(level=logging.INFO)

    # run tests
    unittest.main()
